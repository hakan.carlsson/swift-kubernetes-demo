import Vapor

func throwDie(_ upper: Int) -> Int {
    return Int.random(in: 1 ... upper)
}

func routes(_ app: Application) throws {
    app.get { req in
        return "Dice Thrower version 0.1.2. See /help for API."
    }

    app.get("help") { req -> String in
        //return app.routes.all
        return "Not yet implemented."
    }
    
    // Endpoint for healthcheck
    app.get("healthz") { req -> String in
        return "ok"
    }
    
    app.get("v1alpha1", "dice", ":sides") { req -> String in
        guard let sides: Die = req.parameters.get("sides", as: Die.self) else {
            throw Abort(.badRequest)
        }
        
        var result: Int
        
        switch sides {
        case .d4:
            result = throwDie(4)
        case .d6:
            result = throwDie(6)
        case .d8:
            result = throwDie(8)
        case .d10:
            result = throwDie(10)
        case .d12:
            result = throwDie(12)
        case .d20:
            result = throwDie(20)
        }
        
        return "\(result)"
    }
}
