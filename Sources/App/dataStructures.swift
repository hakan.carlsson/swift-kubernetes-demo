
// Enum declaring the various die options
enum Die: String {
    case d4, d6, d8, d10, d12, d20
}

// To conform to LosslessStringConvertible protocol,
// needed by the route parser in Vapor
extension Die: LosslessStringConvertible {
    public init? (_ description: String) {
        self.init(rawValue: description.lowercased()) // HTTP should be case insensitive
    }
    public var description: String {
        return self.rawValue
    }
}
