# Dice Thrower Swift K8s Demo
This is a small demo app that can be installed in Minikube. It's a web API that simulates a die roll.

## Kubernetes features used
This app is deployed as a K8s *Deployment.* It implements Liveness and Readiness probes, a rollout strategy for upgrades, and has in place resource requests and limits to enable autoscaling in a future excercise.

All Kubernetes related files are in the kubernetes subfolder. The Liveness and Readiness probes are provided by an API endpoint: */healthz*. It's implemented like this:
```swift
// Endpoint for healthcheck
app.get("healthz") { req -> String in
    return "ok"
}
```
