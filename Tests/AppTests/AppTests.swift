@testable import App
import XCTVapor

final class AppTests: XCTestCase {
    /* This test asserts that the /healthz endpoint used by K8s liveness and
     * readiness probes is working. If this test passes, the application is not
     * immediately crashing.
     */
    func testHealthzEndpoint() throws {
        let app = Application(.testing)
        defer { app.shutdown() }
        try configure(app)

        try app.test(.GET, "healthz") { res in
            XCTAssertEqual(res.status, .ok)
            XCTAssertEqual(res.body.string, "ok")
        }
    }
}
